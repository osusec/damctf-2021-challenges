# hav3-i-b33n-pwn3d

Description:

```
I want to check if any of my passwords have been stolen, but I don't want to
risk sending them to [Have I Been Pwned](https://haveibeenpwned.com/Passwords).
I want a stronger guarantee than the [k-anonymity](https://blog.cloudflare.com/validating-leaked-passwords-with-k-anonymity/) that they provide.
You can help me by submitting any leaked passwords you may find to a [private set intersection protocol](https://eprint.iacr.org/2021/1159).

Usage: `./psi_sender.py todo: list of example passwords including 1 real password`
```

Flag:

```
dam{I_DoN'T_KnOw_1f-i-wAs-pwN3D_8U7-1_$ur3-@M-nOW}
```

## Checklist for Author

* [ ] Challenge compiles
* [ ] Dockerfile has been updated
* [ ] `container_src/run_chal.sh` has been updated
* [ ] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [ ] There is at least one test exploit/script in the `tests` directory
* [ ] Update `.dockerignore` with files that won't necessitate a container rebuild

## Info

_Author, put whatever you want here_
