#!/usr/bin/env sage

from common import *
from pwn import *

passwords = ['password1', 'password2', 'password3']

p = remote('hav3-i-b33n-pwn3d.damctf.xyz', 30944)

def send(msg):
    p.sendline(msg.encode())

def recv():
    return p.recvline().decode().strip()

def main():
    a = sample_R()
    m = a*base_p

    send(str(list(m.xy())))

    x_poly = R(recv())
    y_poly = R(recv())

    poly = R(y_poly**2 - x_poly**3 -486662*x_poly**2 - x_poly)

    hashes = []
    for root in poly.roots():
        try:
            hashes.append(int.to_bytes(int(root[0]), byteorder='little', length=16).hex())
        except OverflowError:
            pass

    print('\n'.join(hashes))


if __name__ == "__main__":
    main()
