#!/bin/sh

exec 2>/dev/null

# dir
cd /home/chal

# timeout after 20 sec
# @AUTHOR: make sure to set the propery entry point
#                             <---| don't touch anything left
#
timeout -k1 20 stdbuf -i0 -o0 -e0 sage psi_receiver.py
#           ^^ 20 sec timeout
