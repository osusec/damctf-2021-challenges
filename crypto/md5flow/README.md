# md5flow

Description:

```
Ok, so maybe that [artisan hash](https://gitlab.com/osusec/damctf-2020/-/tree/master/crypto/hashflow) wasn't such a great idea.
Let's use a standard cryptographic hash instead.
```

Flag:

```
dam{1_wOUldN'T-tru$t-MDS_7O-TaKE_MY_t3MP3r47UrE}
```

## Checklist for Author

* [x] Challenge compiles
* [x] Dockerfile has been updated
* [x] `container_src/run_chal.sh` has been updated
* [x] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [x] There is at least one test exploit/script in the `tests` directory
* [x] Update `.dockerignore` with files that won't necessitate a container rebuild

## Info

_Author, put whatever you want here_
