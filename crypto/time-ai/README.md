# time-ai

Description:

```
Crown Sterling, of TIME AI™ fame, has recently publicized a new [Lite Paper](https://www.crownsterling.io/wp-content/uploads/2021/09/Crown-Sterling-Lite-Paper-.pdf).
Alongside their new cryptocurrency, they announced an exciting and novel encryption scheme:
"Leveraging new discoveries in compression technology and mathematical constants,
the team of mathematicians at Crown Sterling have engineered CrownEncryptOTP™, an
enhanced One-Time Pad encryption technology that allows for practical and scalable
use of One-Time Pads."
They claim that their technique is "designed to exploit the by-default randomness of irrational numbers.".
```

Flag:

```
dam{GRoUNdBR3AKIn6-Fl@g_THa7-h@CK3r$_havE_b3EN-Se@r<h1N9_FOr-siNCE-7HE-D@wn_0f-c0mPutiNg}
```

## Checklist for Author

* [ ] Challenge compiles
* [ ] Dockerfile has been updated
* [ ] `container_src/run_chal.sh` has been updated
* [ ] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [ ] There is at least one test exploit/script in the `tests` directory
* [ ] Update `.dockerignore` with files that won't necessitate a container rebuild

## Info

_Author, put whatever you want here_
