#!/usr/bin/env sage

from sage.modules.free_module_integer import IntegerLattice

R.<x> = ZZ['x']

#
#0 < npsn < NPSN_max
#constraint = x^2 - nspn == 0
#
#1/2 < mantissa < 1
#mantissa^2 - npsn/(4^exponent) == 0
#
#y = mantissa * 2^(start_index + known_msg_bits)
#
#z = int(y/2^known_msg_bits)
#full_keystream = y - z * 2^known_msg_bits
#
#
#y^2 - npsn*4^(start_index + known_msg_bits - exponent) == 0
#


def xor(a, b):
    return bytes([x ^^ y for x, y in zip(a, b)])

with open('output.txt', 'r') as f:
    enc_message = bytes.fromhex(f.readlines()[-1].split(" ")[1])
message_bits = len(enc_message) * 8

message = b"Psst! Bob, don't tell anyone, but the flag is dam{"


known_keystream = xor(enc_message, message)

known_message_bits = len(known_keystream) * 8

known_keystream = int.from_bytes(known_keystream, byteorder="big")

#full_keystream^2 + z * full_keystream * 2^(known_message_bits + 1) + z^2 * 4^known_message_bits - npsn*4^(start_index + known_msg_bits - exponent) == 0
#t = z^2 - npsn * 4^(start_index - exponent)
#full_keystream^2 + z * full_keystream * 2^(known_message_bits + 1) + t * 4^known_message_bits == 0

# integer variables: z, t
#
# Constrains: z, t are small

lattice = []

#  w non-integer component of full_keystream
# |w| < 1

#full_keystream = (w + known_keystream)
#(w + known_keystream)^2 + z * (w + known_keystream) * 2^(known_message_bits + 1) + z^2 * 4^known_message_bits - npsn*4^(start_index + known_msg_bits - exponent)
#w^2 + 2*w*known_keystream + known_keystream^2 + z * (w + known_keystream) * 2^(known_message_bits + 1) + z^2 * 4^known_message_bits - npsn*4^(start_index + known_msg_bits - exponent)

# Approximation
#abs(known_keystream^2 + z * known_keystream * 2^(known_message_bits + 1) + z^2 * 4^known_message_bits) - npsn*4^(start_index + known_msg_bits - exponent)) < 1 + 2*known_keystream + z * 2^(known_message_bits + 1) # is small
#abs(known_keystream^2 + z * known_keystream * 2^(known_message_bits + 1) + t * 4^known_message_bits) < 1 + 2*known_keystream + 2^(start_index + known_message_bits + 1) # is small
#
#NPSN_max = 10 * 2^255 + 8
# Size constraints
#npsn < NPSN_max
#z < 2^start_index
#|t| < 1 + 2^(start_index+1)


col0_scale = 2^(known_message_bits + 1)
col1_scale = 2^known_message_bits
col2_scale = 1
col3_scale = 2^(512 + known_message_bits + 1)

#(y/2^known_message_bits)^2 - npsn*4^(start_index - exponent) == 0
#|z^2 - npsn * 4^(start_index - exponent)| < 1 + 2*z
#|t| < 1 + 2*z
#|t| < 1 + 2^(start_index+1)

lattice = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]

lattice[0][0] = 1 * col0_scale # z
lattice[1][1] = 1 * col1_scale # t
lattice[2][3] = 1 * col3_scale # 1


lattice[0][2] = known_keystream * 2^(known_message_bits + 1) * col2_scale
lattice[1][2] = 4^known_message_bits * col2_scale
lattice[2][2] = known_keystream^2 * col2_scale


lattice = IntegerLattice(lattice, lll_reduce=False)
print(lattice)

basis = lattice.HKZ()
#sol = lattice.shortest_vector()

print("HKZ:")
print(basis)
#print(sol)
print(col3_scale)

sol = basis[-1]
assert(sol[3] == col3_scale)
z = sol[0] // col0_scale
t = sol[1] // col1_scale
print(z, t)
print(z.nbits(), t.nbits())

#full_keystream^2 + z * full_keystream * 2^(known_message_bits + 1) + t * 4^known_message_bits == 0
#full_keystream = (sqrt(z^2 - t) - z) * 2^(known_message_bits)
full_keystream = -t * 2^(known_message_bits) / (sqrt(z^2 - t) + z) #(more numerically stable)
full_keystream = N(full_keystream, message_bits + 32) # make sure intermediate rounding doesn't mess it up.
key_stream = (full_keystream * 2^(message_bits - known_message_bits)).round()
key_stream = int(key_stream).to_bytes(len(enc_message), 'big')

message = xor(enc_message, key_stream)
print(message)
