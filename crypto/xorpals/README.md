# xorpals

Description:

```
One of the 60-character strings in the provided file has been encrypted by single-character XOR. 
The challenge is to find it, as that is the flag.

Hint: Always operate on raw bytes, never on encoded strings. Flag must be submitted as UTF8 string.
```

Flag:

```
dam{antman_EXPANDS_inside_tHaNoS_never_sinGLE_cHaR_xOr_yeet}
```

## Checklist for Author
Not needed as I am only serving one static file
```
* [ ] Challenge compiles
* [ ] Dockerfile has been updated
* [ ] `container_src/run_chal.sh` has been updated
* [ ] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [ ] There is at least one test exploit/script in the `tests` directory
* [ ] Update `.dockerignore` with files that won't necessitate a container rebuild
```

## Info

Note to CTF Admins:

This solver script will give two different outputs sometimes, only one is the correct flag:

```
sandbox/damctf2021_chals/xorpals
➜ go run .
ohai
[*]  2124283e242b3128242b1a001d15040b01161a2c2b362c21201a310d240b2a161a2b203320371a362c2b0209001a260d24171a3d0a371a3c20203138
         XOR'd w/ 'e' (ASCII 101)
         DAM[ANTMANexpandsINSIDEThAnOsNEVERSINgleChArXoRYEET]
```

```
sandbox/damctf2021_chals/xorpals
➜ go run .
ohai
[*]  2124283e242b3128242b1a001d15040b01161a2c2b362c21201a310d240b2a161a2b203320371a362c2b0209001a260d24171a3d0a371a3c20203138
         XOR'd w/ 'E' (ASCII 69)
		 dam{antman_EXPANDS_inside_tHaNoS_never_sinGLE_cHaR_xOr_yeet}
```
