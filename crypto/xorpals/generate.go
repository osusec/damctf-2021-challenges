package main

import (
	"encoding/hex"
	"fmt"
	"math/rand"
	"time"
)

func randStr(n int) string {
	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_")
	s := make([]rune, n)
	rand.Seed(time.Now().UnixNano())
	for i := range s {
		s[i] = letters[rand.Intn(len(letters))]
	}
	return string(s)
}

// generate will create the static file that will be given to the players of DAM CTF.
// This file will contain several 60 character XOR'd messages in hex.
// One of the lines will be single charcter XOR'd -- The user must find this line as it is the flag.
// ** Put the output in flags.txt file for CTF ***
func generate() {
	// Generate random flags and place our actual flag within that (hardcored location for now)
	// Flags are hex encoded
	actual_flag := "dam{antman_EXPANDS_inside_tHaNoS_never_sinGLE_cHaR_xOr_yeet}"
	flags := make([]string, 99)

	for i := range flags {
		if i == 42 {
			// Single char XOR -- Operate on bytes
			bytes, _ := hex.DecodeString(hex.EncodeToString([]byte(actual_flag)))
			var byte_flag []byte
			for x := 0; x < len(bytes); x++ {
				byte_flag = append(byte_flag, bytes[x]^byte(69))
			}

			flags[i] = hex.EncodeToString(byte_flag)

		} else {
			// Random XOR -- Operate on bytes
			bytes, _ := hex.DecodeString(hex.EncodeToString([]byte(string("dam{" + randStr(55) + "}"))))
			var byte_flag []byte
			for x := 0; x < len(bytes); x++ {
				byte_flag = append(byte_flag, bytes[x]^byte(rand.Intn(128)))
			}

			flags[i] = hex.EncodeToString(byte_flag)
		}

		fmt.Println(flags[i])
	}

}
