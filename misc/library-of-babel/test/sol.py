#!/usr/bin/python3

"""
usage: python sol.py path/to/world/folder
"""

from collections import defaultdict
from math import floor
from nbt.nbt import *
import anvil
import os
import pyblock
import re
import subprocess
import sys

# Define the path to the world
world_dir = sys.argv[1]

# Initialize the editor
editor = pyblock.MCEditor(world_dir)

coords_output = subprocess.run(
    "pyblock find --coords -184 138 --radius 100 --block lectern".split(),
    env=dict(os.environ, MINECRAFTWORLD=world_dir),
    capture_output=True,
)

coords = [map(int, c.split()) for c in coords_output.stdout.splitlines()[2:-3]]


def coord_to_chunk(x, y, z):
    return (floor(x / 16), floor(y / 16), floor(z / 16))


def chunk_to_section(x, z):
    return (floor(x / 32), floor(z / 32))


chunks = defaultdict(lambda: None)

for x, y, z in coords:
    lectern = editor.get_block(x, y, z)

    if lectern.properties["has_book"].value == "true":

        xc, yc, zc = coord_to_chunk(x, y, z)
        xr, zr = chunk_to_section(xc, zc)

        chunk = chunks[(xc, yc, zc)]
        if not chunk:
            region = anvil.Region.from_file(f"{world_dir}/region/r.{xr}.{zr}.mca")
            chunk = region.get_chunk(xc, zc)
            chunks[(xc, yc, zc)] = chunk

        lectern_te = chunk.get_tile_entity(x, y, z)

        if lectern_te:
            for p in lectern_te["Book"]["tag"]["pages"]:
                for l in p.value.replace("\\n", "\n").splitlines():
                    if re.match("dam{.+}", l):
                        print(l)
                        exit(0)
