# library-of-babel

Description:

```
Legend has it there is a secret somewhere in this library. Unfortunately, all
of our Babel Fishes have gotten lost and the books are full of junk.

*Note: You do not need a copy of Minecraft to play this challenge.*

The flag is in standard flag format.
```

Flag:

```
dam{b@B3l5-b@bBL3}
```

## Checklist for Author

* [x] There is at least one test exploit/script in the `tests` directory

## Info

There are ~5500 books in the world on lecterns, all full of gibberish. One of
the books has the flag in it.

The player needs to extract the NBT BlockEntity data (where the lectern book
text is stored) and find the one line that matches flag format. There are
several libraries available to parse the `.mca` world data files.

The flag is super short due to the length of a line in a Minecraft book.

An example solve script is provided under `test/`.
