# impersonator-3

Description:

```
Amazing work figuring out who this "hacker" really is!
Unfortunately, we have no leads as to what their current whereabouts could possibly be.

Can you try and deduce the address of where they are staying?
Maybe you can find some vacation photos to help you out.

Submit flag as dam{full_name_of_hotel_as_it_appears_on_google}
```

Flag:

```
dam{copthorne_tara_hotel_london_kensington}
```

## Solution
Full name to find twitter account -> use photos to figure out he is in london -> red + blue on train = subway -> subway…? But its above ground in the photo? -> Closer inspection shows its the circle line route -> use subway information to find places where circle line is above ground -> correlate this data with the positioning of churches and hotels near the rail line -> triangulate the church and the station to find the hotel the photo was taken from
