# impersonator-2

Description:

```
Great work figuring out their next moves! 
We have gone ahead and preemptively secured the aforementioned services. 
Now let’s start to try and put this whole thing to rest. 
Can you find the full name of the person who was behind the operation?
Submit the flag as dam{firstname_lastname}
```

Flag:

```
dam{pax_vallejo}
```


## Solution

Start with the github account from previous part -> github api to find patch data -> get email the user used with commit -> Capture gmail packets when creating email to get usreid of account -> full name associated with gmail account with map contribs by appending user ID. Alternatively, there is a tool that automates this called Epieos or Ghunt.
