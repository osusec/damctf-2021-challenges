# rules

Description:

```
Rules check challenge. 

"Innovative"
  - Steve Jobs (probably)

"Bold, Daring"
  - Elon Musktache (probably)

"Never once in my years of CTF have I seen a sanity check with the flag in the rules"
  - Nobody (lmao)
```

Flag:

```
dam{s4niTy_ch3ck_1n_rul35?!}
```

## Checklist for Author

* [x] Flag is actually in rules
* [x] Flag is here
* [x] Flags match!?

## Info

A novel approach to sanity checks where unlike anyone ever, we "hide" it in the rules so people have to open the rules to find the flag to get this easy challenge.