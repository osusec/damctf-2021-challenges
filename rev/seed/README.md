# seed

Description:

```
Having a non-weak seed when generating "random" numbers is super important!
Can you figure out what is wrong with this PRNG implementation?

`seed.py` is the Python script used to generate the flag for this challenge.
`log.txt` is the output from the script when the flag was generated.

What is the flag?
```

Flag:

```
dam{f6f73f022249b67e0ff840c8635d95812bbb5437170464863eda8ba2b9ff3ebf}
```

## Checklist for Author

Checklist items don't apply as this chal only serves static files.

```
* [ ] Challenge compiles
* [ ] Dockerfile has been updated
* [ ] `container_src/run_chal.sh` has been updated
* [ ] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [ ] There is at least one test exploit/script in the `tests` directory
* [ ] Update `.dockerignore` with files that won't necessitate a container rebuild
```

## Info

```
PRNG seed fun!

The goal is to get the player to realize that the seed is vulnerable as it is rounded based on the time. If they work backwards with the time seed, they will eventually find the flag.

The log.txt file serves as a reference to see what the output was leading up to the flag creation.

I based this on a challenge that I did for the snykcon CTF over the summer.
```


`<3 m0x`
