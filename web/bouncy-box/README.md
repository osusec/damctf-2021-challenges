# bouncy-box

Description:

```
Blind sqli on first login, dump password for VIP account using LIKE in query
and brute forcing one character at a time.
```

Flag:

```
dam{b0uNCE_B0UNcE_b0uncE_B0uNCY_B0unce_b0Unce_b0Unc3}
```

## Checklist for Author

* [x] Challenge compiles
* [x] Dockerfile has been updated
* [x] `container_src/run_chal.sh` has been updated
* [x] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [x] There is at least one test exploit/script in the `tests` directory
* [x] Update `.dockerignore` with files that won't necessitate a container rebuild

## Info

See [solve script](tests/e.py)
