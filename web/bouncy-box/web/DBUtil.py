import mysql.connector
import hashlib
import random

class DBUtil:
    def __init__(self, config):
        self.config = config

    def begin_sql_connection(self):
        connection = mysql.connector.connect(**(self.config), buffered=True)
        connection.autocommit = True 
        return connection
    
    def end_sql_connection(self, connection):
        connection.close()
        
def sql_fetchone(connection, sql, val=None):
    connection.ping(reconnect=True, attempts=2, delay=2)
    cursor = connection.cursor()
    if val:
        cursor.execute(sql, val)
    else:
        cursor.execute(sql)
    ret = cursor.fetchone()
    
    cursor.close()
    return ret

def sql_fetchall(connection, sql, val=None):
    connection.ping(reconnect=True, attempts=2, delay=2)
    cursor = connection.cursor()
    if val:
        cursor.execute(sql, val)
    else:
        cursor.execute(sql)
    ret = cursor.fetchall()
    cursor.close()
    return ret

def sql_commit(connection, sql, val=None):
    connection.ping(reconnect=True, attempts=2, delay=2)
    cursor = connection.cursor()
    if val:
        cursor.execute(sql, val)
    else:
        cursor.execute(sql)
    connection.commit()
    ret = cursor.lastrowid
    cursor.close()
    return ret
