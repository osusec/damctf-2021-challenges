#!/usr/bin/env python3

from flask import Flask, render_template, send_file, request, Response
import hashlib
from DBUtil import *
from config import *
from datetime import datetime, timedelta

#from waitress import serve

#import logging
#logger = logging.getLogger('waitress')
#logger.setLevel(logging.ERROR)

app = Flask(__name__)
db = DBUtil(DB_CONFIG)
logindb = DBUtil(DB_LOGINUSER_CONFIG)

@app.route("/")
def index():
    return send_file("game.html")

@app.route("/scoreboard")
def scoreboard():
    connection = db.begin_sql_connection()
    score_res = sql_fetchall(connection, "SELECT username, high_score, games_played, joined FROM users ORDER BY high_score DESC")
    db.end_sql_connection(connection)
    scores = []
    for i, s in enumerate(score_res):
        vip = 'VIP' if s[3] < datetime.now() - timedelta(days=3*365) else ''
        scores += [[
            i + 1,
            s[0],
            s[1],
            s[2],
            vip
        ]]
    return render_template(
        "scoreboard.html",
        scores=scores
    )

@app.route("/login", methods=['POST'])
def login():
    login_connection = logindb.begin_sql_connection()
    connection = db.begin_sql_connection()
    username = None
    password = None
    try:
        username = request.form['username_input']
        password = request.form['password_input']
    except:
        pass
    if username != None and password != None:
        res = sql_fetchone(login_connection, "SELECT username, high_score, games_played, joined FROM users WHERE username='%s' and password='%s'" % (username, password))
        logindb.end_sql_connection(login_connection)
        db.end_sql_connection(connection)
        if res == None:
            return Response("Incorrect username or password!", status=403, mimetype="text/plain")
        else:
            return render_template(
                "stats.html",
                username = res[0],
                highscore = res[1],
                gamesplayed = res[2],
                datejoined = res[3].strftime("%m/%d/%Y"),
                vip = res[3] < datetime.now() - timedelta(days=3*365)
            )
    else:
        content = request.json
        res = sql_fetchone(login_connection, "SELECT username, high_score, games_played, joined, id FROM users WHERE username='%s' and password='%s'" % (content["username"], content["password"]))
        if res == None:
            logindb.end_sql_connection(login_connection)
            db.end_sql_connection(connection)
            return Response("Incorrect username or password!", status=403, mimetype="text/plain")
        else:
            # record high score
            games_played = (res[2] + 1) % 2147483648
            score = res[1] if res[1] > content["score"] else content["score"] % 2147483648
            print(content["score"])
            sql_commit(connection, "UPDATE users SET high_score=%s, games_played=%s WHERE id=%s", val=(score, games_played, res[4]))
            logindb.end_sql_connection(login_connection)
            db.end_sql_connection(connection)
            return Response("Logging you in...", status=200, mimetype="text/plain")

@app.route("/flag", methods=['POST'])
def flag():
    login_connection = logindb.begin_sql_connection()
    username = None
    password = None
    try:
        username = request.form['username_input']
        password = request.form['password_input']
    except:
        pass
    if username != None and password != None:
        res = sql_fetchone(login_connection, "SELECT username, high_score, games_played, joined FROM users WHERE username=%s and password=%s", val=(username, password))
        logindb.end_sql_connection(login_connection)
        if res == None:
            return Response("Incorrect username or password!", status=403, mimetype="text/plain")
        elif res[3] < datetime.now() - timedelta(days=3*365):
            flag = open('./flag').readline().strip()
            return Response(flag, status=200, mimetype="text/plain")
        else:
            return Response("Not a VIP!", status=403, mimetype="text/plain")
    else:
        content = request.json
        res = sql_fetchone(login_connection, "SELECT username, high_score, games_played, joined FROM users WHERE username=%s and password=%s", val=(content["username"], content["password"]))
        logindb.end_sql_connection(login_connection)
        if res == None:
            return Response("Incorrect username or password!", status=403, mimetype="text/plain")
        else:
            return Response("Logging you in...", status=200, mimetype="text/plain")

@app.route('/static/<path:path>')
def send_static(path):
    return send_from_directory('static', path)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
    #serve(app, host='0.0.0.0', port=5000)