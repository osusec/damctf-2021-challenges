function login_pressed() {
    createPostRequest(
        {
            username: document.getElementById("username_input").value,
            password: document.getElementById("password_input").value,
        },
        "/flag",
        () => {
            let f = document.getElementById("login");
            f.action = "/flag";
            f.submit();
        }
    );
}

function createPostRequest(jsonObj, route, callback, reqType='POST')
{
    let request = new XMLHttpRequest()
    request.open(reqType, route)
    request.setRequestHeader('Content-Type','application/json')

    requestBody = JSON.stringify(jsonObj)

    request.addEventListener('load', function(event) {
        let msg = event.target.response;
        let label = document.getElementById("info_label");
        if (event.target.status === 403) {
            label.style.color = "red";
            label.textContent = msg;
        }
        else if(event.target.status !== 200)
        {
            label.style.color = "red";
            label.textContent = msg;
        }
        else
        {
            label.style.color = "white";
            label.textContent = msg;
            callback();
        }
        label.style.display = "flex";
    })
    request.send(requestBody)
}

function show_flag_modal() {
    document.getElementById("modal-backdrop").style.display = "flex";
    document.getElementById("login-button").addEventListener("click", login_pressed);
}
