DROP DATABASE IF EXISTS scores;
CREATE DATABASE scores;
USE scores;

CREATE TABLE users (
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    high_score INT NOT NULL DEFAULT 0,
    games_played INT NOT NULL DEFAULT 0,
    joined TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);

INSERT INTO users (username, password, high_score, games_played, joined) VALUES ("boxy_mcbounce", "B0UncYBouNc3", 1337, 1337, "2018-10-02 01:58:26");
INSERT INTO users (username, password, high_score, games_played, joined) VALUES ("BobbySinclusto", "P@$$w0rD12!", 31337, 31337, "2018-10-02 13:12:31");
INSERT INTO users (username, password, high_score, games_played) VALUES ("azurediamond", "OxQ92r8Fq3", 4, 3);
INSERT INTO users (username, password, high_score, games_played) VALUES ("sergeantGeech", "supersecure", 23, 2);
INSERT INTO users (username, password, high_score, games_played) VALUES ("testbaboon", "password", 9, 6);
INSERT INTO users (username, password, high_score, games_played) VALUES ("toxic_Y", "supersecure", 21, 9);
INSERT INTO users (username, password, high_score, games_played) VALUES ("Aqcurate", "76JMuqr4UD", 3, 1);
INSERT INTO users (username, password, high_score, games_played) VALUES ("BaboonWithTheGoon", "password", 14, 9);
INSERT INTO users (username, password, high_score, games_played) VALUES ("hm3k", "asdf", 2, 9);
INSERT INTO users (username, password, high_score, games_played) VALUES ("Lance Roy", "E4Ef6mhfj5", 20, 9);
INSERT INTO users (username, password, high_score, games_played) VALUES ("lyellread", "asdf", 13, 4);
INSERT INTO users (username, password, high_score, games_played) VALUES ("m0x", "OxQ92r8Fq3", 25, 13);
INSERT INTO users (username, password, high_score, games_played) VALUES ("perchik", "supersecure", 15, 10);
INSERT INTO users (username, password, high_score, games_played) VALUES ("Toxic_Z", "76JMuqr4UD", 18, 15);
INSERT INTO users (username, password, high_score, games_played) VALUES ("captainGeech", "E4Ef6mhfj5", 10, 18);
INSERT INTO users (username, password, high_score, games_played) VALUES ("WholeWheatBagels", "76JMuqr4UD", 19, 3);

CREATE USER 'loginuser'@'%' IDENTIFIED BY 'wx1byHyWFXWFE74YI59YEomOuAGksw7t';
GRANT select ON scores.users TO 'loginuser'@'%';
