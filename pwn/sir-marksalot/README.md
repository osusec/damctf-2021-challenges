# sir-marksalot

Description:

```
Similar to magic marker, but this time you have to find the address of the start
of the maze in memory, get back into the maze without crashing the program,
write shellcode, overwrite the return address, and get eaten by a grue.
```

Flag:

```
dam{1n73N710N4LLy_93771n9_3473n_8y_4_9rU3-7H47_w42_9Ru3S0M3}
```

## Checklist for Author

* [x] Challenge compiles
* [x] Dockerfile has been updated
* [x] `container_src/run_chal.sh` has been updated
* [x] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [x] There is at least one test exploit/script in the `tests` directory
* [x] Update `.dockerignore` with files that won't necessitate a container rebuild

## Info

The solve script works about 20% of the time, might have to run it a few times
before it works (I didn't program it to avoid the grue while writing shellcode
and stuff so sometimes it gets eaten prematurely)