#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define MAZE_WIDTH 40
#define MAZE_HEIGHT 40

struct room {
  char label[28];
  // Use the 4 least significant bits to indicate North, East, South, and West.
  // 0bxxxxNESW
  int directions;
};

void play_maze();
void generate_maze(struct room [MAZE_HEIGHT][MAZE_WIDTH]);
void print_maze(struct room [MAZE_HEIGHT][MAZE_WIDTH], int, int);

int main() {
  char input[32];

  srand(time(NULL));

  setbuf(stdin, NULL);
  setbuf(stdout, NULL);

  printf(
    "After making it out of the maze with your trusty Magic Marker, you've\n"
    "become quite famous in the great empire of Zonk.\n" 
    "In fact, the king himself has made you his knight, and your title is now\n"
    "Sir MarksALot!\n\n"
    "Unfortunately, the king doesn't treat his knights very well. There are\n"
    "rumors of a grue prowling in one of the many underground mazes in the Zonk\n"
    "empire, and your mission is to find it and destroy it. You tried to tell the\n"
    "king that there is no way you're ever going back into a maze, but it's too\n"
    "late. As you stand in the middle of another beautiful forest on a piece of\n"
    "ground that feels rather insubstantial, you savor your last few moments\n"
    "being above-ground.\n"
    "What would you like to do?\n"
  );

  while (1) {
    fgets(input, 32, stdin);

    if (strcmp(input, "jump up and down\n") == 0) {
      printf(
        "Oh no! The ground gives way and you fall into a dark maze.\n"
        "Unfortunately, the hole above you is still much too high to reach.\n"
        "Also unfortunately, the piece of paper that floats into your hand\n"
        "carries no mention of free flags.\n"
        "And the most unfortunate of all: it smells like a grue has been prowling\n" 
        "down here. Better keep an eye out!\n"
        "Luckily, you still have your magnificently magestic magic marker, and\n"
        "you've even made some upgrades to it!\n\n"
      );

      play_maze();
      return 0;
    }
    else {
      printf("I'm not sure I understand.\n");
    }
  }

  return 0;
}

void play_maze() {
  struct room maze[MAZE_HEIGHT][MAZE_WIDTH] = {0};
  struct room *grue_ptr;
  int volatile g_col;
  int volatile g_row;
  int p_col;
  int p_row; 
  char input;

  p_row = rand() % MAZE_HEIGHT;
  p_col = rand() % MAZE_WIDTH;
  g_row = rand() % MAZE_HEIGHT;
  g_col = rand() % MAZE_WIDTH;

  grue_ptr = &(maze[g_row][g_col]);

  generate_maze(maze);

  while (1) {
    // Check for grue
    if (&(maze[p_row][p_col]) == grue_ptr) {
      printf("You were eaten by a Grue.\n");
      return;
    }
    printf("This room has exits to the \n");
    if (maze[p_row][p_col].directions & 0b00001000) {
      printf("North\n");
    }
    if (maze[p_row][p_col].directions & 0b00000100) {
      printf("East\n");
    }
    if (maze[p_row][p_col].directions & 0b00000010) {
      printf("South\n");
    }
    if (maze[p_row][p_col].directions & 0b00000001) {
      printf("West\n");
    }
    
    if (maze[p_row][p_col].label[0] != '\x00') {
      printf("On the wall is written: %s\n", maze[p_row][p_col].label);
    }

    printf("\nWhat would you like to do? (w - go north, a - go west, s - go south, d - go east, x - write something, m - show map): ");
    input = fgetc(stdin);
    while (input != '\n' && fgetc(stdin) != '\n');

    switch (input) {
      case 'w':
        if (maze[p_row][p_col].directions & 0b00001000) {
          p_row -= 1;
        }
        else {
          printf("There's a wall there.\n");
        }
        break;
      case 'a':
        if (maze[p_row][p_col].directions & 0b00000001) {
          p_col -= 1;
        }
        else {
          printf("There's a wall there.\n");
        }
        break;
      case 's':
        if (maze[p_row][p_col].directions & 0b00000010) {
          p_row += 1;
        }
        else {
          printf("There's a wall there.\n");
        }
        break;
      case 'd':
        if (maze[p_row][p_col].directions & 0b00000100) {
          p_col += 1;
        }
        else {
          printf("There's a wall there.\n");
        }
        break;
      case 'x':
        printf("Your magnificently magestic magic marker magically manifests itself in your hand. What would you like to write?\n");
        fgets(maze[p_row][p_col].label, 33, stdin);
        break;
      case 'm':
        print_maze(maze, p_row, p_col);
        break;
      default:
        printf("I'm not sure I understand.\n");
    }
  }
}

void generate_maze(struct room grid[MAZE_HEIGHT][MAZE_WIDTH]) {
  // initialize the run set to be empty
  int runs_cols[MAZE_WIDTH];
  int runs_idx = 0;
  int row = 0;
  int col = 0;
  int rand_col = 0;

  // first row is always a long corridor
  for (; col < MAZE_WIDTH - 1; ++col) {
    grid[0][col].directions |= 0b00000100;
    grid[0][col + 1].directions |= 0b00000001;
  }

  row = 1;
  col = 0;

  while (row < MAZE_HEIGHT) {
    // Add the current cell to the run set
    runs_cols[runs_idx] = col;
    runs_idx += 1;
    
    // Make sure we're not on the last room in the row
    if (col < MAZE_WIDTH - 1 && rand() % 2 == 1) {
      // carve east
      grid[row][col].directions |= 0b00000100;
      grid[row][col + 1].directions |= 0b00000001;
      // make new cell the current cell and repeat
      col += 1;
    }
    else {
      // carve north instead.
      rand_col = runs_cols[rand() % runs_idx];
      grid[row][rand_col].directions |= 0b00001000;
      grid[row - 1][rand_col].directions |= 0b00000010;
      // choose any one of the cells in the run set and carve a passage north.
      // empty run set, move to next room and repeat.
      runs_idx = 0;
      col += 1;
    }
    if (col >= MAZE_WIDTH) {
      runs_idx = 0;
      col = 0;
      row += 1;
    }
  }
}

void print_maze(struct room grid[MAZE_HEIGHT][MAZE_WIDTH], int p_row, int p_col) {
  int row, col = 0;
  char c = ' ';
  for (row = 0; row < MAZE_HEIGHT; ++row) {
    for (col = 0; col < MAZE_WIDTH; ++col) {
      if (grid[row][col].directions & 0b00001000) {
        printf("+   ");
      }
      else {
        printf("+---");
      }
    }
    printf("+\n");
    for (col = 0; col < MAZE_WIDTH; ++col) {
      if (row == p_row && col == p_col) {
        c = '*';
      }
      else if (grid[row][col].label[0] != '\x00') {
        c = '.';
      }
      else {
        c = ' ';
      }
      if (grid[row][col].directions & 0b00000001) {
        printf("  %c ", c);
      }
      else {
        printf("| %c ", c);
      }
    }
    printf("|\n");
  }
  for (col = 0; col < MAZE_WIDTH; ++col) {
    printf("+---");
  }
  printf("+\n");
}
