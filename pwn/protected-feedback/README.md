# protected-feedback

Description:

```
A very uncontrived setup for storing user ratings! (Low memory fragmentation!).
Give good ratings for a free flag!

Compiled with CFI + Safestack.
-fno-stack-protector -fsanitize=safe-stack -fsanitize=cfi -flto -fvisibility=default
```

Flag:

```
dam{l1k3_ST3aLiNg_4_5tAcK_4DDr_fr0M_4_b4BY}
```

## Checklist for Author

* [ ] Challenge compiles
* [ ] Dockerfile has been updated
* [ ] `container_src/run_chal.sh` has been updated
* [ ] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [ ] There is at least one test exploit/script in the `tests` directory
* [ ] Update `.dockerignore` with files that won't necessitate a container rebuild

## Info

Protected with CFI+Safestack. Overwrite function ptr to mmap to leak location of safe stack. Then overwrite function ptr to arbitrary read to leak return address's location. Use 2 byte primitive to redirect control flow into middle of flag reading function.
