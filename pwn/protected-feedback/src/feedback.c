#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>

unsigned long map_addr = 0x600000;
unsigned long num_maps = 0;

struct lucky {
    unsigned long (*lucky_ptr)(unsigned long a, unsigned long b, struct lucky*);
};

unsigned long map_region(unsigned long a, unsigned long b, struct lucky* lucky_struct) {
    unsigned long out = (unsigned long ) mmap((void *) a, b, PROT_READ|PROT_WRITE, MAP_ANONYMOUS|MAP_PRIVATE, -1, 0);
    printf("Mapped rating @ %p\n", (void *) out);
    return out;
}

unsigned long print_rating(unsigned long addr, unsigned long dummy, struct lucky* lucky_struct) {
    unsigned long* out = (unsigned long *) addr;
    printf("Rating: %lu/%lu\n", out[0], out[1]);
    return 0;
}

unsigned long invalid_option(unsigned long dummy1, unsigned long dummy2, struct lucky* lucky_struct) {
    puts("That's not a valid option!\n");
    return 0;
}

unsigned long store_rating(unsigned long num, unsigned long denom, struct lucky* lucky_struct) {
    lucky_struct->lucky_ptr = map_region;
    unsigned long* out = (unsigned long *) lucky_struct->lucky_ptr(map_addr, 0x1000, lucky_struct);
    map_addr += 0x1000; 
    *out = num;
    *(out+1) = denom;
    num_maps += 1;
    printf("Stored Rating: IDX #%lu\n", num_maps);
    return 0;
}

unsigned long read_rating(unsigned long idx, unsigned long dummy, struct lucky* lucky_struct) {
    lucky_struct->lucky_ptr = print_rating;
    if (idx > 0 && idx <= num_maps) {
        lucky_struct->lucky_ptr(0x600000 + 0x1000 * (idx-1), dummy, lucky_struct);
    }
    return 0;
}

unsigned long get_input() {
    char buf[10];
    gets(buf);
    return atol(buf);
}

void menu() {
    struct lucky lucky_struct;
    //unsigned long (*f)(unsigned long a, unsigned long b, struct lucky);
    
    unsigned long option;
    unsigned long arg1;
    unsigned long arg2;
    unsigned short payload;
    // Pass around my lucky struct for extra security!

    puts("Please rate DAM CTF! We'll store your ratings in a secure place.\n");
    while (1) {
        lucky_struct.lucky_ptr = invalid_option;
        printf("0. Store rating (arg1=num/arg2=denom).\n");
        printf("1. Read rating (arg1=idx)\n");
        printf("2. Quit\n");
        option = get_input();

        puts("Input arg1 (if needed)");
        arg1 = get_input();

        puts("Input arg2 (if needed)");
        arg2 = get_input();
        switch (option) {
            case 0:
                lucky_struct.lucky_ptr = store_rating;
                break;
            case 1:
                lucky_struct.lucky_ptr = read_rating;
                break;
            case 2:
                goto quit;         
            default:
                break;
        }

        lucky_struct.lucky_ptr(arg1, arg2, &lucky_struct);
        puts("");
    } 

    quit:
    puts("I'll let you sign 2 bytes to any part of the binary as a thanks for playing.");
    puts("Where do you want to write?");
    arg1 = get_input();

    puts("What do you want to write?");
    payload = (unsigned short) get_input();
    *((unsigned short *) arg1) = payload;
}

int not_get_flag() {
    char buf[40];
    int x = 0;
    volatile int check = 0;
    if (check == 1) {
        FILE* f = fopen("./flag", "r");
        while (x != EOF) {
            x = fgetc(f);
            putchar(x);
        }
    }
    return 0;
}

int main() {
    menu();
    return 0;
}
