# magic-marker

Description:

```
Time based random seed for starting position, can overwrite an int to move any
direction regardless of room exits, traverse memory to get to return address
(bypass canary by abusing 2d array indexing properties), overwrite return
address with win function, get flag
```

Flag:

```
dam{m4rvellOU5lY_M49n1f1cen7_m491C_m4rker5_M4KE_M4zE_M4n1PuL471oN_M4R91N4llY_M4L1c1Ou5}
```

## Checklist for Author

* [x] Challenge compiles
* [x] Dockerfile has been updated
* [x] `container_src/run_chal.sh` has been updated
* [x] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [x] There is at least one test exploit/script in the `tests` directory
* [x] Update `.dockerignore` with files that won't necessitate a container rebuild

## Info

Might make a harder version of this time permitting
