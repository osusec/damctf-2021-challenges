#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define MAZE_WIDTH 40
#define MAZE_HEIGHT 40

struct room {
  char label[28];
  // Use the 4 least significant bits to indicate North, East, South, and West.
  // 0bxxxxNESW
  int directions;
};

void play_maze();
void win();
void generate_maze(struct room [MAZE_HEIGHT][MAZE_WIDTH]);
void print_maze(struct room [MAZE_HEIGHT][MAZE_WIDTH], int, int);

int main() {
  char input[32];

  srand(time(NULL));

  setbuf(stdin, NULL);
  setbuf(stdout, NULL);

  printf(
    "Welcome to Zonk, the best text based adventure ever created!\n\n"
    "It's a beautiful day. Birds are singing, and a gentle breeze whistles through the trees. "
    "Suddenly, the wind shifts, the birds stop singing, and the ground under your feet begins to feel rather insubstantial.\n\n"
    "What would you like to do?\n"
  );

  while (1) {
    fgets(input, 32, stdin);

    if (strcmp(input, "jump up and down\n") == 0) {
      printf(
        "Oh no! The ground gives way and you fall into a dark maze.\n"
        "Unfortunately, the hole above you is much too high to reach.\n"
        "But what's this? There's a piece of paper on the ground!\n"
        "It looks like an advertisement for some free flags, and you can just\n"
        "barely make out the map that's sketched on the back.\n"
        "Free flags are pretty cool, but all you really want to do is get out "
        "of this maze!\n"
        "Luckily, you brought your magnificently magestic magic marker that\n"
        "you can use to write labels on anything!\n"
        "Maybe it'll help you find the way out...\n\n"
      );

      play_maze();
      return 0;
    }
    else {
      printf("I'm not sure I understand.\n");
    }
  }

  return 0;
}

void play_maze() {
  struct room maze[MAZE_HEIGHT][MAZE_WIDTH] = {0};
  int p_row = rand() % MAZE_HEIGHT;
  int p_col = rand() % MAZE_WIDTH;
  char input;

  generate_maze(maze);

  while (1) {
    printf("This room has exits to the \n");
    if (maze[p_row][p_col].directions & 0b00001000) {
      printf("North\n");
    }
    if (maze[p_row][p_col].directions & 0b00000100) {
      printf("East\n");
    }
    if (maze[p_row][p_col].directions & 0b00000010) {
      printf("South\n");
    }
    if (maze[p_row][p_col].directions & 0b00000001) {
      printf("West\n");
    }
    
    if (maze[p_row][p_col].label[0] != '\x00') {
      printf("On the wall is written: %s\n", maze[p_row][p_col].label);
    }

    printf("\nWhat would you like to do? (w - go north, a - go west, s - go south, d - go east, x - write something, m - show map, q - give up): ");
    input = fgetc(stdin);
    while (input != '\n' && fgetc(stdin) != '\n');

    switch (input) {
      case 'w':
        if (maze[p_row][p_col].directions & 0b00001000) {
          p_row -= 1;
        }
        else {
          printf("There's a wall there.\n");
        }
        break;
      case 'a':
        if (maze[p_row][p_col].directions & 0b00000001) {
          p_col -= 1;
        }
        else {
          printf("There's a wall there.\n");
        }
        break;
      case 's':
        if (maze[p_row][p_col].directions & 0b00000010) {
          p_row += 1;
        }
        else {
          printf("There's a wall there.\n");
        }
        break;
      case 'd':
        if (maze[p_row][p_col].directions & 0b00000100) {
          p_col += 1;
        }
        else {
          printf("There's a wall there.\n");
        }
        break;
      case 'x':
        printf("Your magnificently magestic magic marker magically manifests itself in your hand. What would you like to write?\n");
        fgets(maze[p_row][p_col].label, 33, stdin);
        break;
      case 'm':
        print_maze(maze, p_row, p_col);
        break;
      case 'q':
        return;
      default:
        printf("I'm not sure I understand.\n");
    }
  }
}

void generate_maze(struct room grid[MAZE_HEIGHT][MAZE_WIDTH]) {
  // initialize the run set to be empty
  int runs_cols[MAZE_WIDTH];
  int runs_idx = 0;
  int row = 0;
  int col = 0;
  int rand_col = 0;

  // first row is always a long corridor
  for (; col < MAZE_WIDTH - 1; ++col) {
    grid[0][col].directions |= 0b00000100;
    grid[0][col + 1].directions |= 0b00000001;
  }

  row = 1;
  col = 0;

  while (row < MAZE_HEIGHT) {
    // Add the current cell to the run set
    runs_cols[runs_idx] = col;
    runs_idx += 1;
    
    // Make sure we're not on the last room in the row
    if (col < MAZE_WIDTH - 1 && rand() % 2 == 1) {
      // carve east
      grid[row][col].directions |= 0b00000100;
      grid[row][col + 1].directions |= 0b00000001;
      // make new cell the current cell and repeat
      col += 1;
    }
    else {
      // carve north instead.
      rand_col = runs_cols[rand() % runs_idx];
      grid[row][rand_col].directions |= 0b00001000;
      grid[row - 1][rand_col].directions |= 0b00000010;
      // choose any one of the cells in the run set and carve a passage north.
      // empty run set, move to next room and repeat.
      runs_idx = 0;
      col += 1;
    }
    if (col >= MAZE_WIDTH) {
      runs_idx = 0;
      col = 0;
      row += 1;
    }
  }
}

void print_maze(struct room grid[MAZE_HEIGHT][MAZE_WIDTH], int p_row, int p_col) {
  int row, col = 0;
  char c = ' ';
  for (row = 0; row < MAZE_HEIGHT; ++row) {
    for (col = 0; col < MAZE_WIDTH; ++col) {
      if (grid[row][col].directions & 0b00001000) {
        printf("+   ");
      }
      else {
        printf("+---");
      }
    }
    printf("+\n");
    for (col = 0; col < MAZE_WIDTH; ++col) {
      if (row == p_row && col == p_col) {
        c = '*';
      }
      else if (grid[row][col].label[0] != '\x00') {
        c = '.';
      }
      else {
        c = ' ';
      }
      if (grid[row][col].directions & 0b00000001) {
        printf("  %c ", c);
      }
      else {
        printf("| %c ", c);
      }
    }
    printf("|\n");
  }
  for (col = 0; col < MAZE_WIDTH; ++col) {
    printf("+---");
  }
  printf("+\n");
}

void win() {
  printf("Congratulations! You escaped the maze and got the flag!\n");
  FILE* flagfile = fopen("flag", "r");
  char flag[100];
  fgets(flag, 100, flagfile);
  printf("%s\n", flag);
  fclose(flagfile);
}
