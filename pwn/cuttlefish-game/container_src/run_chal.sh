#!/bin/sh

# no stderr
exec 2>/dev/null

# dir
cd /chal

# timeout after 10 minutes
# @AUTHOR: make sure to set the propery entry point
#                             <---| don't touch anything left
#                                 | unless you need a longer timeout
timeout -k1 600 stdbuf -i0 -o0 -e0 python -c 'import pty;pty.spawn("./run.sh")'
#           ^^ 10 min timeout
