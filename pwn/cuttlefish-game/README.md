# cuttlefish-game

Description:

```
There is strategy in kernel exploitation. First off, having a good primitive is very important.

The arbitrary read is at the front and keeps an eye on how the pwner is performing. And the rest of the bugs focuses on the back of the information leak and follows their lead.

Compiled Linux v5.15 with the following options...

CONFIG_PAGE_TABLE_ISOLATION=y
CONFIG_SECURITY_DMESG_RESTRICT=y
CONFIG_STATIC_USERMODEHELPER=y
CONFIG_STATIC_USERMODEHELPER_PATH=""
CONFIG_SLAB_FREELIST_HARDENED=y
CONFIG_SLUB=y
```

Flag:

```
dam{a_B1t_0f_4_cl1ch3d_r3f4reNc3}
```

## Checklist for Author

* [ ] Challenge compiles
* [ ] Dockerfile has been updated
* [ ] `container_src/run_chal.sh` has been updated
* [ ] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [ ] There is at least one test exploit/script in the `tests` directory
* [ ] Update `.dockerignore` with files that won't necessitate a container rebuild

## Info

Kernel exploitation with SMEP+KPTI+SMAP enabled.

```
CONFIG_PAGE_TABLE_ISOLATION=y
CONFIG_SECURITY_DMESG_RESTRICT=y
CONFIG_STATIC_USERMODEHELPER=y
CONFIG_STATIC_USERMODEHELPER_PATH=""
CONFIG_SLAB_FREELIST_HARDENED=y
CONFIG_SLUB=y
```

One bit heap-overflow into ref counter to get UAF. Overwrite function pointer, stack pivot and KROP to root.
