# cookie-monster

Description:

```
babypwn by zach and allen and lance
Use format string to leak canary, overwrite return address to call
system("/bin/bash") (/bin/bash is in .rodata)
```

Flag:

```
dam{s74CK_c00k13S_4r3_d3L1C10Us}
```

## Checklist for Author

* [x] Challenge compiles
* [x] Dockerfile has been updated
* [x] `container_src/run_chal.sh` has been updated
* [x] `build-artifacts` has been updated to include any files needed from the remote service (i.e. libc)
    * If there are no files needed from the container, please **delete** this file to speed up CI/CD runs
* [x] There is at least one test exploit/script in the `tests` directory
* [x] Update `.dockerignore` with files that won't necessitate a container rebuild

## Info

See [solve script](e.py)
