#include <stdio.h>
#include <stdlib.h>

const char a[] = "/bin/sh";

void bakery() {
    char input[32];
    printf("Enter your name: ");
    fgets(input, 32, stdin);
    printf("Hello ");
    printf(input);
    printf("Welcome to the bakery!\n\nCurrent menu:\n");
    system("cat cookies.txt");
    printf("\nWhat would you like to purchase?\n");
    fgets(input, 64, stdin);
    printf("Have a nice day!\n");
}
int main() {
    setbuf(stdin, NULL);
    setbuf(stdout, NULL);
    bakery();
    return 0;
}